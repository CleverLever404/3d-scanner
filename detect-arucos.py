#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 14 22:40:17 2022

@author: chimaka
"""

# %% BLOQUE  DE IMPORTS
import numpy as np
import cv2 as cv
from glob import glob

# el diccionario a usar, correspondioente a los tags impresos
arDic = cv.aruco.getPredefinedDictionary(cv.aruco.DICT_5X5_50)

# cargo la lista de los archivos de imagen
imgFilesList = glob("/home/chimaka/Documents/scripts/3d-scanner/Test-Images/*.jpg")
imgFilesList.sort()

# %% BLOQUE DETECCION ARUCO

# keep looping for all the files
for imgFile in imgFilesList:
    # Read image
    frame = cv.imread(imgFile)

    # detect the marke in this image
    corners, ids, rejectedImgPoints = cv.aruco.detectMarkers(frame, arDic)

    # print the ids of the markers detected, to check that it's ok
    print(ids)

    # if there were markers detected, then draw them in the image
    if not isinstance(ids, type(None)):
        frame = cv.aruco.drawDetectedMarkers(frame, corners, ids)

    # display the image
    cv.imshow("frame", frame)

    c = cv.waitKey() & 0xFF
    # if the 'q' key is pressed, break from the loop
    if c == ord('q'):
        break
    else: # continue to next iteration
        continue

# close all open windows
cv.destroyAllWindows()
