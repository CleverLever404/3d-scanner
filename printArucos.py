#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jan 22 20:31:16 2022

guardar en disco imagenes de tags arucos

@author: sebalander
"""
# %%

import cv2 as cv

arDic = cv.aruco.getPredefinedDictionary(cv.aruco.DICT_5X5_50)
sidePixels = 500
nMarkers = 10


for i in range(nMarkers):
    print("marcador id:", i)
    img = arDic.drawMarker(i, sidePixels)

    nombreArchivo = "arucoMarker_5x5_50_%03d.jpeg"%i
    print(nombreArchivo)
    cv.imwrite(nombreArchivo, img)

    cv.imshow("marcador", img)
    c = cv.waitKey(1000)
    if c == ord("q"):
        break
